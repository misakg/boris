// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('starter', ['ionic','ngMap','google.places'])

.run(function($http,$ionicPlatform,$rootScope) {
  $rootScope.trenutnaLokacija = [];
  $rootScope.trenutnaLokacija.latitude ='';
  $rootScope.trenutnaLokacija.longitude = '';
  $ionicPlatform.ready(function() {
    
    var bgGeo = window.BackgroundGeolocation;

    var callbackFn = function(location, taskId) {
      
      console.log(location);

      //https://oblutak.website/boris/add.php?lat=0&lng=0&speed=0&user=1




            var coords      = location.coords;
            var timestamp   = location.timestamp;
            var latitude    = coords.latitude;
            var longitude   = coords.longitude;
            var speed       = coords.speed;
            $rootScope.brzina=speed;
            $rootScope.trenutnaLokacija=coords;


      $http.get('https://oblutak.website/boris/add.php?lat='+latitude+'&lng='+longitude+'&speed='+speed+'&user=1').success(function(data){

      });

      bgGeo.finish(taskId);
    };

    var failureFn = function(error) {
        console.log('BackgroundGeoLocation error', error);

    }

    // Configure the plugin
    bgGeo.configure({
      desiredAccuracy: 0,
      stationaryRadius: 50,
      distanceFilter: 50,
      disableElasticity: false, 
      locationUpdateInterval: 1000,
      minimumActivityRecognitionConfidence: 10,   
      fastestLocationUpdateInterval: 1000,
      activityRecognitionInterval: 10000,
      locationAuthorizationRequest: 'Always',
      stopTimeout: 15,
      activityType: 'AutomotiveNavigation',
      foregroundService: true,
      // Application config
      debug: false,
      forceReloadOnLocationChange: false,
      forceReloadOnMotionChange: true,  
      forceReloadOnGeofence: false,
      forceReloadOnBoot: true,      
      stopOnTerminate: false,            
      startOnBoot: true   
    }, function(state) {
      if (!state.enabled) {
        bgGeo.start();  
      }
    });


    bgGeo.on('location', callbackFn, failureFn);



    if (window.cordova && window.Keyboard) {
      window.Keyboard.hideKeyboardAccessoryBar(true);
    }

    if (window.StatusBar) {

      StatusBar.styleDefault();
    }
  });
})

.controller('ctsCrt', function($scope, $rootScope, $http) {


  $scope.showDetail = function(e, shop) {
    vm.shop = shop;
    vm.map.showInfoWindow('vremeInfoWindow', this);
  };

navigator.geolocation.getCurrentPosition(onSuccess, onError);

function onSuccess(position) {
        $rootScope.trenutnaLokacija.latitude = position.coords.latitude 
        $rootScope.trenutnaLokacija.longitude = position.coords.longitude 
       
    }

function onError(){}



    $scope.$on('mapInitialized', function(event, map) {
      navigator.geolocation.getCurrentPosition(onSuccess, onError);

function onSuccess(position) {
        $rootScope.trenutnaLokacija.latitude = position.coords.latitude 
        $rootScope.trenutnaLokacija.longitude = position.coords.longitude 
       
    }

function onError(){}
            console.log('mapa event '+ event);
            $rootScope.map = map;
            $scope.currentCenter = '['+$rootScope.trenutnaLokacija.latitude+','+$rootScope.trenutnaLokacija.longitude+']';

            console.log($scope.currentCenter );
    });

});
