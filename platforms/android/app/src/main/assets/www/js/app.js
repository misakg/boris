// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('starter', ['ionic'])

.run(function($http,$ionicPlatform) {
  $ionicPlatform.ready(function() {

    var bgGeo = window.BackgroundGeolocation;

    var callbackFn = function(location, taskId) {
      console.log(location);
      bgGeo.finish(taskId);
    };

    var failureFn = function(error) {
        console.log('BackgroundGeoLocation error', error);

    }

    // Configure the plugin
    bgGeo.configure({
      desiredAccuracy: 0,
      stationaryRadius: 50,
      distanceFilter: 50,
      disableElasticity: false, 
      locationUpdateInterval: 1000,
      minimumActivityRecognitionConfidence: 10,   
      fastestLocationUpdateInterval: 1000,
      activityRecognitionInterval: 10000,
      locationAuthorizationRequest: 'Always',
      stopTimeout: 15,
      activityType: 'AutomotiveNavigation',
      foregroundService: true,
      // Application config
      debug: false,
      forceReloadOnLocationChange: false,
      forceReloadOnMotionChange: true,  
      forceReloadOnGeofence: false,
      forceReloadOnBoot: true,      
      stopOnTerminate: false,            
      startOnBoot: true   
    }, function(state) {
      if (!state.enabled) {
        bgGeo.start();  
      }
    });


    bgGeo.on('location', callbackFn, failureFn);


    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs).
    // The reason we default this to hidden is that native apps don't usually show an accessory bar, at
    // least on iOS. It's a dead giveaway that an app is using a Web View. However, it's sometimes
    // useful especially with forms, though we would prefer giving the user a little more room
    // to interact with the app.
    if (window.cordova && window.Keyboard) {
      window.Keyboard.hideKeyboardAccessoryBar(true);
    }

    if (window.StatusBar) {
      // Set the statusbar to use the default style, tweak this to
      // remove the status bar on iOS or change it to use white instead of dark colors.
      StatusBar.styleDefault();
    }
  });
})
